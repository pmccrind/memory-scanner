/////////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02 - memscan - EE 491F - Spr 2022
///
/// @file    ms.h
/// @version 1.5 - changed scanStruct to only accept a Map
///
/// ms - prints out all valid memeory addresses
///
/// @author  Patrick McCrindle <pmccrind@hawaii.edu>
/// @date    09_Feb_2022
///
///////////////////////////////////////////////////////////////////////////////

#define VERSION "\n ms 1.1 \n Copyright Patrick McCrindle\n"
#define FILENAME "ms"

//Define the map Structure for ms.c and ms_functions.c
typedef struct {

   //beginning address
   char startAddress[50];

   //ending address
   char endAddress[50];

   //Permissions
   char permissions[10];
   
   //store the path
   char path[2048];
} Map;


//Define the result structure to hold the result from the scan 
//function
typedef struct {
   
   //Size of the Memory Space
   int size;
   
   //Number of Capital 'A''s found when scanning
   int capitalA;

} Result;

//Scans the memory range in the structure, returning the results
//as a Result structure
Result scanStruct( Map incomingMap );

//Prints out each Line, passing all values as pointers
void printLine( int* numOfLines, Map* incomingMap, int* difference, int* capA );


