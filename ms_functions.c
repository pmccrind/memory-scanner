/////////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02 - memscan - EE 491F - Spr 2022
///
/// @file    ms_functions.c
/// @version 1.4 - Spilt the Scan and print functions.
///
/// ms_functions - Holds required functions for ms
///
/// @author  Patrick McCrindle <pmccrind@hawaii.edu>
/// @date    09_Feb_2022
///
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <string.h>
#include "ms.h"

//Scan the memory addresses that are stored in each struct
//return a result structure with the required information
Result scanStruct( Map incomingMap ){
  
   //Structure that will return the result from the scan
   //function
   Result results = { 0, 0 };

   
   //first convert the start of the address range into a pointer
   
   //placeholder for conversion
   unsigned long placeholder;

   //copy into the placeholder from the struct as a hex integer
   sscanf(incomingMap.startAddress, "%lx", &placeholder);
  
   //startTest now holds the beginning of the address range
   //as a char pointer so we can step through byte
   //by byte
   char* startTest = (char *) placeholder;
  

   //same thing for the ending address

   //zero out placeholder
   placeholder = 0;

   //copy into the placeholder as a hex integer
   sscanf(incomingMap.endAddress, "%lx", &placeholder);
   
   //endTest now hold the ending address to check
   char* endTest = (char* ) placeholder;

   //holds the number of bytes between start and end
   //that need to be tested
   results.size = ( endTest - startTest ) * sizeof(char);   
   

   //check if we have permission to access this section
   //of memeory
   if ( incomingMap.permissions[0] != 'r' ) {
      //not allowed to scan return to the main program
      
      //nothing was read thus 0 out the number of bytes
      results.size = 0;
      
      //return the result structure
      return results;   
   
   }
   
   //check the path for the [vvar] memory section 
   //paths are terminated with a new line character
   if ( strcmp(incomingMap.path, "[vvar]\n" ) == 0  ) {
      
      //nothing was read thus 0 out the number of bytes
      results.size = 0;
      
      //return the results
      return results;

   }

   //Memory Address is valid, need to validate and scan
   //for capital A characters.
   

   //Step through the range byte by  byte to verify access
   //record any 'A' characters that are seen
   for ( int i = 0; i < results.size; i++ ){
      
      //test if the address was valid
      if ( *startTest == 'A'  ){
         //accessing the de-referenceed pointer
         //will seg fault if we are not allowed access to it
         
         //increment the results capital A value
         results.capitalA++;
      }  
      
      //increment to the next byte
      startTest++;
   
   }//end validation of memory for loop
   
   //Entire range has been scanned with no errors.

   //Return the result strucuture
   return results;

}//end scan function



//Prints out a line with the requested format
void printLine( int* numOfLines, Map* incomingMap, int* difference, int* capA ){
   
   //requested output
   printf("%d:\t 0x%-17s - 0x%-17s %s \t Number of bytes read: [%d] \t\t Number of 'A' is [%d]\n", *numOfLines, incomingMap->startAddress, incomingMap->endAddress, incomingMap->permissions, *difference, *capA);

}//End the PrintLine Function




