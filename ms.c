/////////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02 - memscan - EE 491F - Spr 2022
///
/// @file    ms.c
/// @version 1.5 - Parses the file for scanning, saves result in a struct.
///
/// ms - scans the memory address made aviable to this process
///
/// @author  Patrick McCrindle <pmccrind@hawaii.edu>
/// @date    09_Feb_2022
///
///////////////////////////////////////////////////////////////////////////////

//All required includes
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ms.h"

//@argc - argument count passed to the program, expected 2
//@argv - argument list passed to the program, expected ["./ms", "/proc/self/maps"] 
int main( int argc, char *argv[] ) {
   
   //Nothing to scan exit and return failure
   if (argc == 1) {
      
      //print error to standard error
      fprintf(stderr, "%s: Nothing to parse.\n", FILENAME);
      exit(EXIT_FAILURE);
   

   //More then 3 arguments is not expected input.
   } else if (argc >= 3) {

      //print error to standard error
      fprintf(stderr, "%s: Too many Arguments. Expected arguments are %s, and ['proc/self/maps'] and not [%s]\n", FILENAME, FILENAME, argv[2]);
      exit(EXIT_FAILURE);
       
   // Two arguments is the expected input
   } else {
      
      //Check that the incoming command is what we are expecting
      if( !( strcmp( argv[1], "/proc/self/maps" ) == 0 ) ){
         //Not a match exit and return failure
         fprintf(stderr, "%s: [%s] was an incorrect command. Expected ['/proc/self/maps']\n", FILENAME, argv[1]);
         //Exit with Failure
         exit(EXIT_FAILURE);
       
      }
      
      //Command given was as expected open and run the 
      //program.
      
      //File pointer for fopen()
      FILE* filePointer = NULL;

      //Command from input to run which is 
      // /proc/self/maps
      char* command = argv[1];

      //Open the command
      filePointer = fopen(command, "r");

      //Check Opened Successfully
      if( filePointer == NULL ){
         
         //File did not open successfully
         fprintf(stderr, "%s: [%s] could not be opened for processing.\n", FILENAME, command);
         //Exit with failure
         exit(EXIT_FAILURE);

      }

      //Command was opened succesfully
      
      //File pointer to the line
      char* readLine;

      //buffer size needed for a line
      size_t len = 0;

      //size of the line
      ssize_t read;

      //number of lines in the file
      int numLines = 0;
      
      //Stores the whole valid memory addres per line
      char* addressWhole;

      //Stores the begining of the address
      char* addressStart;

      //Store the end of the address
      char* addressEnd;
      
      //Stores permission per line
      char* permission;

      //stores the path so we can check for the 
      //[vvar] path
      char* path;

      //structure used to hold all the required data for each line
      //The Map struct is defined in ms.h it holds:
      //start address, end address, permission, and file path
      //all as strings.
      Map map_0 = {" ", " ", " ", " "};
      
      //Structure used to hold the results from the scanning function
      //The Result structure is defined in ms.h  holds: 
      //number of bytes read, and the number
      //of 'A''s seen while scanning all as ints.
      Result results = { 0, 0 };

      //Print out the header of the program
      printf("Memory Scanner: \n");

      //read in the output from the command line by line
      while ( ( read = getline( &readLine, &len, filePointer ) ) != -1 ){

         //spilt on the first space to capture the beginning and end
         //address together
         addressWhole = strtok( readLine, " " );
         
         //Spilt on the next space to get the permissions
         permission = strtok( NULL, " " );
         
         //Spilt on the next spaces 4 time to get to the path
         path = strtok( NULL, " " );
         path = strtok( NULL, " " );
         path = strtok( NULL, " " );
         //Path is now storing the path section of the command
         path = strtok( NULL, " " );

         //on the first field spilt on "-" save the first part as 
         //beginning address and the second part as the end address

         //Stores Beginning Address
         addressStart = strtok( addressWhole, "-" );
         
         //Stores Ending Address
         addressEnd = strtok( NULL, "-" );

         //Store the above in a struct
         strcpy( map_0.startAddress, addressStart );
         strcpy( map_0.endAddress, addressEnd );
         strcpy( map_0.permissions, permission );
         strcpy( map_0.path, path );
         

         //Send the struct to the scanning function.
         results = scanStruct( map_0 );

         //Print out the information per line
         printLine(&numLines, &map_0, &results.size, &results.capitalA);
         

         //Clear the Map struct out
         strcpy( map_0.startAddress,  " " ); 
         strcpy( map_0.endAddress,  " " ); 
         strcpy( map_0.permissions,  " " ); 
         strcpy( map_0.path,  " " ); 

         //Zero out the results structure
         results.size = 0;
         results.capitalA = 0;

         //Increment the number of lines
         numLines++;

      }//end while loop

      //Used to see if file closed successfully
      int fileClosed = 0;

      fileClosed = fclose(filePointer);
          
      if ( fileClosed != 0  ){
         //Error Closing the File
         fprintf(stderr, "%s: [%s] could not be closed.\n", FILENAME, command);
         
         exit(EXIT_FAILURE);
      }
      //Command Closed Successfully

      //Command Successfully parsed
      exit(EXIT_SUCCESS);

   }//end argc == 2 case
      
   //Should not reach this section,
   //exit with failure if you do.
   exit(EXIT_FAILURE);

}//End Main Function
 

