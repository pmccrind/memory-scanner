###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 02 - memscan - EE 491F - Spr 2022
###
### @file    Makefile
### @version 1.5 - Added make new, which runs clean and then make ms.
###
### Build a memory scanner.
###
### @author  Patrick McCrindle <pmccrind@hawaii.edu>
### @date    01_Feb_2022
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = gcc
CFLAGS = -g -Wall

TARGET = ms

all: $(TARGET)

ms: ms.c
	$(CC) -B  $(CFLAGS) ms_functions.c  -o $(TARGET) ms.c

clean:
	rm -f $(TARGET)

test:
	./ms /proc/self/maps

new: clean
	$(MAKE)
